let path = require('path');

let output = {
  path: path.resolve(__dirname, 'build'),
  filename: 'bundle.js',
  publicPath: '/'
};

module.exports = {
  entry: path.resolve(__dirname, 'js/main.js'),
  output,
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        },
      },
      {
        test:/\.html$/,
        loader: "html-loader"
      },
      {
            test:/\.vue$/,
            loader: "vue"
      }
    ]
  },
  resolve: {
    root: path.resolve(__dirname, './'),
    alias: {
      vue$: 'vue/dist/vue.js'
    }
  },
  devServer: {
    contentBase: path.join(__dirname, 'build'),
    compress: true,
    port: 5533,
    output
  }
};
