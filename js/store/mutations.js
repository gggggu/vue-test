export default {
    SET_USERS (state, users) {
        state.users = users;
    },
    SET_PRODUCTS (state, products){
        state.products = products;
    },
    CHANGE_LOADING (state, loading){
        state.loadingData = loading
    },
    SET_SINGLE_PRODUCT(state, singleProduct){
        state.products.push(singleProduct);
    },

}
