import ProductsApi from '../api/product'
import UsersApi from '../api/user'
import SingleProductApi from '../api/singleproduct'


export default {

    GET_USERS(context) {

        UsersApi.getUserRequest().then((response) => {
            context.commit('SET_USERS', response.data)
        });
    },
    GET_SINGLE_PRODUCT(context,id) {

       return SingleProductApi.getSingleRequest(id).then((response) => {
           console.log('c сервера', response);
            context.commit('SET_SINGLE_PRODUCT', response.data);
        });
    },
    GET_PRODUCTS(context) {

        context.commit('CHANGE_LOADING', true);

        return ProductsApi.getProductsRequest().then((response) => {

            context.commit('SET_PRODUCTS', response.data);

            context.commit('CHANGE_LOADING', false);

            return response.body
        })

    }
}
