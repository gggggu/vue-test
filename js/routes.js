import UsersPage from './components/pages/UsersPage'
import ProductsPage from './components/pages/ProductsPage'
import ProductPage from './components/pages/ProductPage'


// тут мы декларируем все роуты приложения (какие компоненты за какие адреса отвечают)
export default [
    {
        name: 'userspage',
        component: UsersPage,
        path: '/userspage/',
    },
    {
        name: 'productspage',
        component: ProductsPage,
        path: '/productspage/',

    },
    {
        name: 'productpage',
        component: ProductPage,
        path: '/product/:product_id',

    }
]
