import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';


import routes from './routes';
import store from './store';
import App from './components/App';
import spoiler from './helpers/Spoilerfilter'

const router = new VueRouter({ routes });

Vue.router = router;
Vue.use(VueResource);
Vue.use(VueRouter);
Vue.filter('spoiler', spoiler);

export default new Vue({
  el: '#root',
  router,
  store,
  render: (h) => h(App)
});
