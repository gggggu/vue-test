//сортировка по названию а-я
export function sortByNameAsc (a,b){
    if (a.name > b.name) {
        return 1;
    }
    if (a.name < b.name) {
        return -1;
    }
    // a должно быть равным b
    return 0;
};
//сортировка по названию я-а
export function sortByNameDesc (a,b){
    if (a.name < b.name) {
        return 1;
    }
    if (a.name > b.name) {
        return -1;
    }
    // a должно быть равным b
    return 0;
};
//сотировка по стоимости от самых дорогих
export function sortByPriceDesc (a,b){
    if (a.price < b.price) {
        return 1;
    }
    if (a.price > b.price) {
        return -1;
    }
    // a должно быть равным b
    return 0;
}
//сотировка по стоимости от самых дешевых
export function sortByPriceAsc (a,b){
    if (a.price > b.price) {
        return 1;
    }
    if (a.price < b.price) {
        return -1;
    }
    // a должно быть равным b
    return 0;
}
