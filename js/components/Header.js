import Vue from 'vue'

export default Vue.component('header-menu', {
  template: '<div><router-link to="/userspage">Go to Userspage</router-link>\n' +
            '<router-link to="/productspage">Go to Productspage</router-link></div>', // тут нужно показать 2 ссылки при помощь которых можно переключаться между 2мя страницами

});

