import Vue from 'vue'
import template from '../../html/Singleproduct.html'
import arrFilter from '../../helpers/arrFilter'

export default Vue.component('product-page', {
    template,
    created() {
        if (this.productCheckInStorage()){return console.log('первое условие, продукты есть');
        }
        else{
            this.productDowloadAndCheckInServer();
        }

    },
    methods:{
        productCheckInStorage: function(){
            return arrFilter (this.$store.state.products, this.$route.params.product_id).length>0;
        },

        productDowloadAndCheckInServer:function(){

            return this.$store.dispatch('GET_PRODUCTS').then((response)=>{

            if(arrFilter(response,this.$route.params.product_id).length>0){
                return console.log('товары после загрузки с сервера')
            }
            else{
                this.$store.dispatch('GET_SINGLE_PRODUCT', this.$route.params.product_id);

            }
            })
        }

    },

    computed:{
        singleProductFilter: function() {
           return this.$store.state.products.filter((arrElement) => {
                return arrElement.id == this.$route.params.product_id;

            });
        }

    }


});