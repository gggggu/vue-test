import Vue from 'vue'
import VueSlider from 'vue-slider-component';
import Product from '../Product'
import {sortByNameAsc, sortByNameDesc, sortByPriceAsc, sortByPriceDesc} from '../../helpers/sort'
import template from '../../html/ProductsPage.html'

export default Vue.component('products-page', {
    template,
    created() {
        this.$store.dispatch('GET_PRODUCTS').then(this.sortByDefault)

    },
    components: {
        'product': Product,
        VueSlider,
    },
    data: function () {

        return {
            sortType: 'sortbynameasc',
            onclickId: '',
            priceSlider: {
                min: 0,
                max: 0,
                value: []
            }
        }

    },
    computed: {
        productRange: function () {

            return this.$store.state.products.filter((arrElement) => {

                return arrElement.price >= this.priceSlider.value[0] && arrElement.price <= this.priceSlider.value[1]
            });
        },
        productlist: function () {

            let productListArr; //переменная, в которую switch присваивает тип сортировки,используется для вывода в return
            switch (this.sortType) {
                case 'sortbynameasc':
                    productListArr = sortByNameAsc;//a-z
                    break;
                case 'sortbynamedesc':
                    productListArr = sortByNameDesc;//z-a
                    break;
                case 'sortbypriceasc':
                    productListArr = sortByPriceAsc;//от дешевых к дорогим
                    break;
                case 'sortbypricedesc':
                    productListArr = sortByPriceDesc;//от дорогих к дешевым
                    break;
            }
            return this.productRange.sort(productListArr);

        },
    },
    methods: {
        recordDataInOnclickId: function (itemId) {

            this.onclickId = itemId;
            return this.onclickId
        },
        sortByDefault(result) {
            result.sort((a, b) => {

                    if (a.price > b.price) {
                        return 1;
                    }
                    if (a.price < b.price) {
                        return -1;
                    }
                    //сортировка массива от самых дешевых к самым дорогим
                    return 0
                }
            );
            this.priceSlider.min = result[0].price; //добавляем минимальную цену
            this.priceSlider.max = result[result.length - 1].price;//добавляем максимальную
            this.priceSlider.value = [this.priceSlider.min, this.priceSlider.max];//добавляем массив с минимальным и максимальным значением в value
        }
    }

});
