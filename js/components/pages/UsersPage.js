import Vue from 'vue'
import User from '../User'

export default Vue.component('users-page', {

    template:

        '<table><tr v-for="item in $store.state.users">' +
        '<user :user="item"></user></tr></table>',

  created(){
      this.$store.dispatch('GET_USERS');
    },

    components: {
        'user': User
    }
});
