import Vue from 'vue'
import template from '../html/ProductRow.html'


export default Vue.component('product', {
    props: ['product'],
    template,
});
