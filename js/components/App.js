import Vue from 'vue'
import HeaderMenu from './Header'
import template from '../html/App.html'

export default Vue.component('app',{
    template,
    components: {
    'header-menu': HeaderMenu
    }

})
