import Vue from 'vue'


export default Vue.component('user', {
  props: ['user'],
  template:
      '<span>' +
      '<td><img :src = "user.photo" width="100"></td>' +
      '<td>{{user.description}}</td>' +
      '<td>{{user.name}}</td>' +
      '<td>{{user.age}}</td>' +
      '</span>'
});
